<?php

/**
 * Mobile Class
 */
class Mobile
{
	
	function __construct()
	{
		session_start();
		$this->conn = new mysqli("localhost", "root", "", "crud");
	}

	public $id = '';
	public $m_name = '';
	public $m_model = '';
	public $status = '';
	public $details = '';
	public $photo = '';
	
	public function prepar($data)
	{
		if (isset($data['id']) && !empty($data['id'])) {
			$this->id = $data['id'];
		}
		if (isset($data['m_name']) && !empty($data['m_name'])) {
			$this->m_name = $data['m_name'];
		}
		if (isset($data['m_model']) && !empty($data['m_model'])) {
			$this->m_model = $data['m_model'];
		}
		if (isset($data['status']) && !empty($data['status'])) {
			$this->status = $data['status'];
		}
		if (isset($data['details']) && !empty($data['details'])) {
			$this->details = $data['details'];
		}
		if (isset($data['photo']) && !empty($data['photo'])) {
			$this->photo = $data['photo'];
		}
	}

	

	public function store()
	{
		$query = "INSERT INTO `mobile` (`m_name`,`m_model`,`status`,`photo`,`details`) VALUES ('$this->m_name', '$this->m_model', '$this->status','$this->photo','$this->details')";

		$result = $this->conn->query($query);
		
		if($result){
			$_SESSION['massage'] = "<h3 style='color:green;'>Data Successfully Add </h3>";
		}else{
			$_SESSION['massage'] = "<h3 style='color:red;'>Data is not Add </h3>";
		}
		header('location:add.php');
	}

	public function index()
	{
		$query = "SELECT * FROM `mobile`";
		$result = $this->conn->query($query);
		while ($row = $result->fetch_array()) {
			$datas[] = $row;
		}
		return $datas;

	}

	public function show()
	{
		$query = "SELECT * FROM `mobile` WHERE `id`=$this->id";
		$result = $this->conn->query($query);
		$data = $result->fetch_array();
		return $data;
	}

	public function delete()
	{
		$query = "SELECT * FROM `mobile` WHERE `id`=$this->id";
		$result = $this->conn->query($query);
		$data = $result->fetch_array();

		$photo_link = '../../public/picture/'.$data['photo'];

		$query = "DELETE FROM `mobile` WHERE `id`=$this->id";
		$result = $this->conn->query($query);
		unlink($photo_link);

		if($result){
			$_SESSION['massage'] = "<h3 style='color:green;'>Data Successfully Add </h3>";
		}else{
			$_SESSION['massage'] = "<h3 style='color:red;'>Data is not Add </h3>";
		}
		header('location:index.php');
	}

	public function edit()
	{
		$query = "SELECT * FROM `mobile` WHERE `id`=$this->id";
		$result = $this->conn->query($query);
		$data = $result->fetch_array();
		return $data;
	}

	public function update()
	{
		$query = "UPDATE `mobile` SET `m_name` = '$this->m_name', `m_model` = '$this->m_model', `status` = '$this->status', `details` = '$this->details' WHERE `mobile`.`id` = '$this->id'";

		
		print_r($)

		$result = $this->conn->query($query);
		
		if($result){
			$_SESSION['massage'] = "<h3 style='color:green;'>Data Successfully update </h3>";
		}else{
			$_SESSION['massage'] = "<h3 style='color:red;'>Data is Update </h3>";
		}
		header('location:edit.php');
	}


}


?>