<?php 
include('../../Src/Mobile/Mobile.php'); 

$obj = new Mobile;
$datas = $obj->index();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Mobile</title>
</head>
<body>
	
<h1>Mobile Page</h1>

<ul>
	<li><a href="../../dashboard.php"> Home </a></li>
	<li><a href="add.php"> Mobile Add </a></li>
</ul>

<?php
	if (isset($_SESSION['massage']) && !empty($_SESSION['massage'])) {
		echo $_SESSION['massage'];
		$_SESSION['massage'] ="";
	}
?>

	<table border="1px">
		<tr>
			<th>Sl No.</th>
			<th>Mobile Name</th>
			<th>Mobile Model</th>
			<th>Photo</th>
			<th>Manage</th>
		</tr>

	<?php $i = 1; ?>
	<?php foreach ($datas as $data): ?>
		<tr>
			<td><?php echo $i; $i++; ?></td>
			<td><?php echo $data['m_name']; ?></td>
			<td><?php echo $data['m_model']; ?></td>
			<td> <img src="<?php echo '../../public/picture/'.$data['photo'];?>" width="100px"> </td>
			<td>
				<a href="view.php?id='<?php echo $data['id'];?>'">View</a>
				<a href="delete.php?id='<?php echo $data['id'];?>'" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
				<a href="Edit.php?id='<?php echo $data['id'];?>'">Edit</a>
			</td>
		</tr>
	<?php endforeach;?>

	</table>

</body>
</html>