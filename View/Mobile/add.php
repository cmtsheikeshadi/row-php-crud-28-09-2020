<!DOCTYPE html>
<html>
<head>
	<title>Add Mobile</title>

	<!-- Used for ckEditor -->
	<script src="../../plagin/ckeditor/ckeditor.js"></script>

</head>
<body>
	<h1>Mobile Add Page</h1>

<ul>
	<li><a href="../../dashboard.php"> Home </a></li>
	<li><a href="index.php"> Mobile Home </a></li>
</ul>

<?php
	session_start();
	if (isset($_SESSION['massage']) && !empty($_SESSION['massage'])) {
		echo $_SESSION['massage'];
		session_unset(); 
	}
?>

	<form method="POST" action="store.php" enctype="multipart/form-data">
		<input type="text" name="m_name" value="" placeholder="Mobile Name">
		<input type="text" name="m_model" value="" placeholder="Mobile Model">

		<select name="status">
			<option value="1">Select Status</option>
			<option value="1">Public</option>
			<option value="0">Unpublic</option>
		</select>
		<input type="file" name="image">
		
		<textarea name="details" id="editor1" rows="10" cols="80"></textarea>
        <script>
                // Replace the <textarea id="editor1"> with a CKEditor 4
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
        </script>
        <input type="submit" name="submit" value="Submit">

	</form>
	
</body>
</html>