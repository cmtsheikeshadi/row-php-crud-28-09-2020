<?php
include('../../Src/Mobile/Mobile.php');
$obj = new Mobile;
$obj->prepar($_GET);
$data = $obj->edit();


?>

<!DOCTYPE html>
<html>
<head>
	<title>Edit Mobile</title>

	<!-- Used for ckEditor -->
	<script src="../../plagin/ckeditor/ckeditor.js"></script>

</head>
<body>
	<h1>Edit Add Page</h1>

<ul>
	<li><a href="../../dashboard.php"> Home </a></li>
	<li><a href="index.php"> Mobile Home </a></li>
</ul>

<?php
	
	if (isset($_SESSION['massage']) && !empty($_SESSION['massage'])) {
		echo $_SESSION['massage'];
		session_unset(); 
	}
?>

	<form method="POST" action="update.php" enctype="multipart/form-data">
		<input type="text" name="m_name" value="<?php echo $data['m_name']; ?>" placeholder="Mobile Name">

		<input type="hidden" name="id" value="<?php echo $data['id']; ?>">

		<input type="text" name="m_model" value="<?php echo $data['m_model']; ?>" placeholder="Mobile Model">

		<select name="status">
				<?php 
					if($data['status'] == TRUE){
						echo '<option value="1">Public</option>';
						echo '<option value="0">Unpublic</option>';
					}else{
						echo '<option value="0">Unpublic</option>';
						echo '<option value="1">Public</option>';
					}
				?>
				

		</select>
		<input type="file" name="image">
		<img src="../../public/picture/<?php echo $data['photo']; ?>" width="100px">
		
		<textarea name="details" id="editor1" rows="10" cols="80"><?php echo $data['details']; ?></textarea>
        <script>
                // Replace the <textarea id="editor1"> with a CKEditor 4
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
        </script>
        <input type="submit" name="submit" value="Update">

	</form>
	
</body>
</html>